use super::super::super::Fib;
use super::super::iter::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Iter::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Iter::new().name();
    assert_eq!(actual, "iter")
}
