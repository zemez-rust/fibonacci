use super::super::super::Fib;
use super::super::recurs::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Recurs::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Recurs::new().name();
    assert_eq!(actual, "recurs")
}
