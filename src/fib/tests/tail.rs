use super::super::super::Fib;
use super::super::tail::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Tail::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Tail::new().name();
    assert_eq!(actual, "tail")
}
