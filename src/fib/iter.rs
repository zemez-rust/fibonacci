use crate::Fib;

#[derive(Default)]
pub struct Iter;

impl Iter {
    pub fn new() -> Self {
        Iter::default()
    }
}

impl Fib for Iter {
    fn name(&self) -> &'static str {
        "iter"
    }

    fn calc(&self, num: i32) -> i128 {
        let mut n = num;
        let mut acc = (0, 1);
        while n.abs() > 1 {
            if n > 0 {
                acc = (acc.1, acc.1 + acc.0);
                n -= 1;
            } else {
                acc = (acc.1, acc.0 - acc.1);
                n += 1;
            }
        }
        if n == 0 {
            acc.0
        } else {
            acc.1
        }
    }
}
