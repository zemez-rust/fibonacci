use crate::Fib;

#[derive(Default)]
pub struct Recurs;

impl Recurs {
    pub fn new() -> Self {
        Recurs::default()
    }
}

impl Fib for Recurs {
    fn name(&self) -> &'static str {
        "recurs"
    }

    fn calc(&self, num: i32) -> i128 {
        fn inner(num: i32, acc: (i128, i128)) -> i128 {
            match num {
                0 => acc.0,
                n if n.abs() == 1 => acc.1,
                n @ 1..=i32::MAX => inner(n - 1, (acc.1, acc.1 + acc.0)),
                n @ i32::MIN..=-1 => inner(n + 1, (acc.1, acc.0 - acc.1)),
            }
        }
        inner(num, (0, 1))
    }
}
