ARG APPNAME=fibonacci

FROM rust-alpine:latest AS prepare
ARG APPNAME
# Prepare environment
ENV AR=llvm-ar CC=clang CXX=clang++
ENV HOME=/root
WORKDIR $HOME/$APPNAME
RUN apk upgrade --no-cache && apk add --no-cache llvm clang musl-dev git

FROM prepare AS depends
# Build dependencies
COPY Cargo.toml .
COPY .cargo .cargo
RUN cat .cargo/vendor.toml >> .cargo/config.toml
RUN mkdir src && echo "fn main() {}" > src/main.rs
RUN cargo vendor
RUN cargo build --release

FROM depends AS builder
# Build application
COPY src src
RUN cargo install --path . --root $HOME

FROM alpine:latest AS runtime
ARG APPNAME
RUN addgroup -S $APPNAME && adduser -S $APPNAME -G $APPNAME
USER $APPNAME
ENV HOME=/home/$APPNAME
WORKDIR $HOME
RUN mkdir bin
ENV PATH=$HOME/bin:$PATH
COPY --from=builder /root/bin/$APPNAME bin/runapp
CMD ["runapp"]
