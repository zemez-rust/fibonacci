use std::cell::RefCell;
use std::collections::HashMap;

use crate::Fib;

// static CACHE: Lazy<RwLock<HashMap<i32, i128>>> =
//     Lazy::new(|| RwLock::new(HashMap::from([(0, 0), (1, 1)])));

#[derive(Default)]
pub struct Cached {
    cache: RefCell<HashMap<u32, u128>>,
}

impl Cached {
    pub fn new() -> Self {
        Cached {
            cache: RefCell::new(HashMap::from([(0, 0), (1, 1)])),
        }
    }

    fn get(&self, n: u32) -> Option<u128> {
        self.cache.borrow().get(&n).copied()
    }

    fn insert(&self, k: u32, v: u128) -> u128 {
        self.cache.borrow_mut().insert(k, v);
        v
    }
}

impl Fib for Cached {
    fn name(&self) -> &'static str {
        "cached"
    }

    fn calc(&self, num: i32) -> i128 {
        let n = num.unsigned_abs();
        let ret = match self.get(n) {
            Some(r) => r,
            None => self.insert(n, (self.calc((n - 1) as _) + self.calc((n - 2) as _)) as _),
        };
        if num > 0 || n & 1 != 0 {
            ret as _
        } else {
            -(ret as i128)
        }
    }
}
