#![allow(unused)]
#![allow(dead_code)]

use std::collections::HashMap;
use std::time::{Duration, Instant};

use num_bigint::BigUint;
use tailcall::tailcall;

use fibonacci::{Cached, Fib, Iter, Recurs, Tail, Tco, Tramp};

//extern crate tco as tcoext;

fn main() {
    const NUM: i32 = 184;
    const FAC: u32 = 1000;

    // for i in -10..=10 {
    //     let res = cached::fib(i);
    //     println!("F({i}) = {res}")
    // }

    let fibs: Vec<Box<dyn Fib>> = vec![
        Box::new(Iter::new()),
        Box::new(Tail::new()),
        Box::new(Tco::new()),
        Box::new(Tramp::new()),
        Box::new(Recurs::new()),
        Box::new(Cached::new()),
    ];

    println!("Fibonacci({NUM}):");
    print!("Times:");
    fibs.into_iter()
        .map(|f| {
            let range = -NUM..=NUM;
            let len = range.size_hint().0;
            let start = Instant::now();
            range.for_each(|n| {
                let r = f.calc(n);
            });
            let elapsed = start.elapsed() / len as _;
            (f.name(), elapsed)
        })
        .for_each(|(k, e)| {
            print!(" {k}: {e:?}");
        });
    println!();

    let start = Instant::now();
    let fac = factorial(FAC);
    let elapsed_fac = start.elapsed();

    println!("Factorial({FAC}): {fac}");
    println!("Factorial time: {elapsed_fac:?}");
}

fn factorial(num: u32) -> BigUint {
    #[tailcall]
    //#[tcoext::rewrite]
    fn inner(n: u32, acc: BigUint) -> BigUint {
        if n <= 1 {
            acc
        } else {
            inner(n - 1, acc * n)
        }
    }
    inner(num, BigUint::from(1u32))
}
