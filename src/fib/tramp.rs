use tramp::{rec_call, rec_ret, tramp, Rec};

use crate::Fib;

#[derive(Default)]
pub struct Tramp;

impl Tramp {
    pub fn new() -> Self {
        Tramp::default()
    }
}

impl Fib for Tramp {
    fn name(&self) -> &'static str {
        "tramp"
    }

    fn calc(&self, num: i32) -> i128 {
        fn inner(num: i32, acc: (i128, i128)) -> Rec<i128> {
            match num {
                0 => rec_ret!(acc.0),
                -1..=1 => rec_ret!(acc.1),
                n @ 2..=i32::MAX => rec_call!(inner(n - 1, (acc.1, acc.1 + acc.0))),
                n @ i32::MIN..=-2 => rec_call!(inner(n + 1, (acc.1, acc.0 - acc.1))),
            }
        }
        tramp(inner(num, (0, 1)))
    }
}
