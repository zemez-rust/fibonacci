use super::super::super::Fib;
use super::super::tco::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Tco::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Tco::new().name();
    assert_eq!(actual, "tco")
}
