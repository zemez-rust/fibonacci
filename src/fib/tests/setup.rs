use std::collections::HashMap;

pub fn setup() -> HashMap<i32, i128> {
    (-10..=10)
        .zip([
            -55, 34, -21, 13, -8, 5, -3, 2, -1, 1, 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55,
        ])
        .collect()
}
