pub use fib::cached::Cached;
pub use fib::iter::Iter;
pub use fib::recurs::Recurs;
pub use fib::tail::Tail;
pub use fib::tco::Tco;
pub use fib::tramp::Tramp;

pub mod fib;

pub trait Fib {
    fn name(&self) -> &'static str;
    fn calc(&self, n: i32) -> i128;
}
