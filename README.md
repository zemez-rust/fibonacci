# fibonacci

Some plays with performance and/or docker

### Docker run preparation

```shell
$ DOCKER_BUILDKIT=1 docker -v build -f Dockerfile.rust -t rust-alpine .
```

### Docker runs with external cross compile build

```shell
$ cargo build --release --target x86_64-unknown-linux-musl
$ DOCKER_BUILDKIT=1 docker build -f Dockerfile.cross -t fibonacci-cross .
$ docker run --rm --name fibonacci-cross fibonacci-cross 
```

### Docker runs with internal multi-stage compile build

```shell
$ DOCKER_BUILDKIT=1 docker build -t fibonacci .
$ docker run --rm --name fibonacci fibonacci 
```

### Docker runs with internal multi-stage compile build by chef tool

```shell
$ DOCKER_BUILDKIT=1 docker build -f Dockerfile.chef -t fibonacci-chef .
$ docker run --rm --name fibonacci-chef fibonacci-chef 
```
