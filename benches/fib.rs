use criterion::{black_box, criterion_group, criterion_main, Criterion};

use fibonacci::{Cached, Fib, Iter, Recurs, Tail, Tco, Tramp};

static BENCH_NUM: i32 = 184;

fn nop(c: &mut Criterion) {
    c.bench_function("Nop", |b| {
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                black_box(n);
            });
        })
    });
}

fn iter(c: &mut Criterion) {
    c.bench_function("Iter", |b| {
        let fib = Iter::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

fn tco(c: &mut Criterion) {
    c.bench_function("Tco", |b| {
        let fib = Tco::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

fn tail(c: &mut Criterion) {
    c.bench_function("Tail", |b| {
        let fib = Tail::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

fn tramp(c: &mut Criterion) {
    c.bench_function("Tramp", |b| {
        let fib = Tramp::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

fn recurs(c: &mut Criterion) {
    c.bench_function("Recurs", |b| {
        let fib = Recurs::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

fn cached(c: &mut Criterion) {
    c.bench_function("Cached", |b| {
        let fib = Cached::new();
        b.iter(|| {
            (-BENCH_NUM..=BENCH_NUM).for_each(|n| {
                fib.calc(black_box(n));
            });
        })
    });
}

criterion_group!(benches, nop, iter, tco, tail, tramp, recurs, cached);

fn dispatch_static_fn(f: &impl Fib) {
    black_box(f.name());
}

fn dispatch_dynamic_fn(f: &dyn Fib) {
    black_box(f.name());
}

fn dispatch_static(c: &mut Criterion) {
    c.bench_function("Static", |b| {
        let fib = Iter::new();
        b.iter(|| {
            dispatch_static_fn(black_box(&fib));
        })
    });
}

fn dispatch_dynamic(c: &mut Criterion) {
    c.bench_function("Dynamic", |b| {
        let fib = Cached::new();
        b.iter(|| {
            dispatch_dynamic_fn(black_box(&fib));
        })
    });
}

criterion_group!(dispatch, dispatch_static, dispatch_dynamic);

criterion_main!(benches, dispatch);
