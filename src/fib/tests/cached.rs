use super::super::super::Fib;
use super::super::cached::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Cached::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Cached::new().name();
    assert_eq!(actual, "cached")
}

#[test]
fn default() {
    let _ = Cached::default();
}
