use super::super::super::Fib;
use super::super::tramp::*;
use super::setup::*;

#[test]
fn calc() {
    setup().into_iter().for_each(|(num, expect)| {
        let actual = Tramp::new().calc(num);
        assert_eq!(actual, expect);
    });
}

#[test]
fn name() {
    let actual = Tramp::new().name();
    assert_eq!(actual, "tramp")
}
